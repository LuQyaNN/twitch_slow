// Twich_Slowpoke.cpp: ���������� ����� ����� ��� ����������� ����������.
//

//#include "stdafx.h"
#include "emoti.h"

#include "targetver.h"

#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"

#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/SSLManager.h"
#include "Poco/Net/AcceptCertificateHandler.h"
#include "Poco/Util/Application.h"
#include "Poco/URI.h"
#include "Poco/URIStreamOpener.h"
#include "Poco/StreamCopier.h"
#include "Poco/Net/HTTPStreamFactory.h"
#include "Poco/Net/HTTPSStreamFactory.h"

#include "SFML/Graphics.hpp"
#include "SFML/Network.hpp"

#include "boost/algorithm/string.hpp"

#include <unordered_map>
#include <memory>
#include <sstream>
#include <regex>
#include <string>
#include <fstream>
#include <vector>
#include <iostream>
#include <codecvt>
#include <locale>
#include <iterator>
#include <queue>
#include <list>
#include <stdio.h>
#include <tchar.h>


struct Img
{
	
	sf::Image* img;
	std::string url;
	bool isLoaded = false;

};
	


std::string getHttpContent(const std::string& url) {
	std::string content{};
	try {
		auto& opener = Poco::URIStreamOpener::defaultOpener();
		auto uri = Poco::URI{ url };
		auto is = std::shared_ptr<std::istream>{ opener.open(uri) };
		Poco::StreamCopier::copyToString(*(is.get()), content);
	}
	catch (Poco::Exception& e) {
		std::cerr << e.displayText() << std::endl;
	}
	return content;
}

int main(int argc,char* argv[])
{
	
	try {
	
	std::string args;
	for (int i = 1; i < argc; ++i)
	{
		
		args.append(argv[i]); 
		args.append(" ");
	}

	size_t oauth_pos = args.find("oauth=");
	size_t nick_pos = args.find("nick=");
	size_t channel_pos = args.find("channel=");
	
	if (oauth_pos != -1 && nick_pos != -1 && channel_pos != -1)
	{
		setlocale(LC_ALL, "");
		std::wstring_convert<std::codecvt_utf8<wchar_t>> utf8_codec;
		std::map<std::wstring, Img*> emoties;


		sf::RenderWindow window(sf::VideoMode(400, 300), "Twitch Slowpokes");
		sf::Font font;
		font.loadFromFile("c:/Windows/Fonts/arial.ttf");


		sf::TcpSocket socket;

		socket.connect("irc.twitch.tv", 6667);


		sf::String message = "PASS ";
		sf::String pass = args.substr(oauth_pos + 6, args.find_first_of(" ", oauth_pos + 6) - oauth_pos - 6); //"oauth:dqmac1xdo69uu989e3mtqmn5hg0r79";
		std::cout << args.substr(oauth_pos + 6, args.find_first_of(" ", oauth_pos + 6) - oauth_pos - 6) << std::endl;
		message += pass;
		message += "\n";
		socket.send(message.toUtf8().c_str(), message.getSize() + 1);

		message = "NICK ";
		sf::String nick = args.substr(nick_pos + 5, args.find_first_of(" ", nick_pos + 5) - nick_pos - 5); //"luqyann";
		std::cout << args.substr(nick_pos + 5, args.find_first_of(" ", nick_pos + 5) - nick_pos - 5) << std::endl;
		message += nick;
		message += "\n";
		socket.send(message.toUtf8().c_str(), message.getSize() + 1);

		message = "JOIN #";
		sf::String Channel = args.substr(channel_pos + 8, args.find_first_of(" ", channel_pos + 8) - channel_pos - 8);
		std::cout << args.substr(channel_pos + 8, args.find_first_of(" ", channel_pos + 8) - channel_pos - 8) << std::endl;
		message += Channel;
		message += " \n";
		socket.send(message.toUtf8().c_str(), message.getSize() + 1);
		std::string EmotiJson;

		Poco::Net::HTTPStreamFactory::registerFactory();
		Poco::Net::HTTPSStreamFactory::registerFactory();

		// http://stackoverflow.com/questions/18315472/https-request-in-c-using-poco
		Poco::Net::initializeSSL();
		Poco::Net::SSLManager::InvalidCertificateHandlerPtr ptrHandler(new Poco::Net::AcceptCertificateHandler(false));
		Poco::Net::Context::Ptr ptrContext(new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, ""));
		Poco::Net::SSLManager::instance().initializeClient(0, ptrHandler, ptrContext);

		const auto url = "https://api.twitch.tv/kraken/chat/" + Channel + "/emoticons";
		const auto content = getHttpContent(url);
		EmotiJson = content;


		window.setActive(false);
		socket.setBlocking(false);

		rapidjson::Document d;
		d.Parse<rapidjson::kParseDefaultFlags>(EmotiJson.c_str());



		Poco::Net::HTTPClientSession EmoticonsResiever("static-cdn.jtvnw.net");

		for (auto i = d.FindMember("emoticons")->value.Begin();
		i != d.FindMember("emoticons")->value.End(); i++)
		{

			std::string url = i->FindMember("url")->value.GetString();

			Img* tmp = new Img;
			tmp->img = 0;
			tmp->isLoaded = false;
			tmp->url = url;
			std::wstring regex = utf8_codec.from_bytes(i->FindMember("regex")->value.GetString());
			if (regex == L"B-?\\)") regex = L"B)";
			else
			if (regex == L"\\:-?[z|Z|\\|]") regex = L":|";
			else
			if (regex == L"\\:-?\\)") regex = L":)";
			else
			if (regex == L"\\:-?\\(") regex = L":(";
			else
			if (regex == L"\\:-?(p|P)") regex = L":P";
			else
			if (regex == L"\\;-?(p|P)") regex = L";P";
			else
			if (regex == L"\\&lt\\;3") regex = L"<3";
			else
			if (regex == L"\\;-?\\)") regex = L";)";
			else
			if (regex == L"R-?\\)") regex = L"R)";
			else
			if (regex == L"\\:-?D") regex = L":D";
			else
			if (regex == L"\\:-?(o|O)") regex = L":O";
			else
			if (regex == L"\\&gt\\;\\(") regex = L">(";
			else
			if (regex == L"[oO](_|\\.)[oO]") regex = L"O_o";
			else
			if (regex == L"\\:-?[\\\\/]") regex = L":/";
			
			emoties.insert(std::make_pair(regex, tmp));

		}

		std::vector<emoti*> drawable_vector;
		
		std::queue<emoti*> emoti_queue;

		char buffer[1024];
		sf::Clock SpawnTimer;
		sf::Clock TickDeltaTimer;
		sf::Time TickDelta;
		while (window.isOpen())
		{
			sf::sleep(sf::milliseconds(1000 / 60));

			
			
			std::size_t received = 0;

			sf::Socket::Status stat = socket.receive(buffer, sizeof(buffer), received);

			if (received > 0)
			{

				std::string s(buffer, received);

				std::wstring u8str = utf8_codec.from_bytes(s);
				std::wcout << u8str;

				std::vector<std::wstring> u8str_splited;
				boost::split(u8str_splited, u8str, boost::is_any_of(L":"));
				std::wstring chatnick;
				std::wstring chatmessage;
				size_t PRIVMSGpos = u8str.find(L"PRIVMSG");
				if (PRIVMSGpos != -1)
				{

					chatnick = u8str.substr(1, u8str.find(L"!") - 1);
					chatmessage = u8str.substr(u8str.find(L":", PRIVMSGpos) + 1, u8str.size() - u8str.find(L":", PRIVMSGpos));
					std::wcout << "nick: " << chatnick << std::endl;
					std::wcout.clear();
					std::wcout << "message: " << chatmessage << std::endl;
					
					std::wcout.clear();
					
					std::vector<std::wstring> tokens;
					boost::split(tokens, chatmessage, boost::is_any_of("\r\n "));
					for (auto& message_word : tokens)
					{
						
						
						if (emoties.find(message_word) == emoties.end())
							continue;

						auto tmp_1 = emoties.at(message_word);
								if (!tmp_1->isLoaded)
								{
									Poco::Net::HTTPRequest EmoticonsResieverRequest(Poco::Net::HTTPRequest::HTTP_GET, tmp_1->url.erase(0, 27));
									EmoticonsResiever.sendRequest(EmoticonsResieverRequest);
									Poco::Net::HTTPResponse EmoticonsResieverResponse;
									std::string rawimage;
									Poco::StreamCopier::copyToString(EmoticonsResiever.receiveResponse(EmoticonsResieverResponse), rawimage);
									sf::Image* img = new sf::Image;
									if (img->loadFromMemory(rawimage.c_str(), rawimage.size()))
									{
										tmp_1->img = img;
										tmp_1->isLoaded = true;
									}

								}
								

								emoti* tmp_emoti = new emoti(*tmp_1->img, window.getDefaultView().getCenter());
								drawable_vector.push_back(tmp_emoti);
								emoti_queue.push(tmp_emoti);
				
					}
				}
				else if (u8str_splited[0] == L"PING")
				{
					socket.send(L"PONG tmi.twitch.tv\n", sizeof(L"PONG tmi.twitch.tv\n"));
				}
			}



			sf::Event event;
			while (window.pollEvent(event))
			{

				if (event.type == sf::Event::Closed)
					window.close();
			}

			if (SpawnTimer.getElapsedTime().asMilliseconds() >= 200)
			{
				if (emoti_queue.size() > 0)
				{
					emoti_queue.front()->Spawn();
					emoti_queue.pop();
				}
				SpawnTimer.restart();
			}

			window.clear(sf::Color(128, 0, 255));
			for (size_t i = 0; i < drawable_vector.size(); ++i)
			{
				if (!drawable_vector[i]->isSpawned())
					continue;
				drawable_vector[i]->Tick(TickDelta);

				window.draw(drawable_vector[i]->sprite);
				if (drawable_vector[i]->life_time.getElapsedTime().asSeconds() >= 3)
				{
					delete drawable_vector[i];
					drawable_vector.erase(drawable_vector.begin() + i);
				}

			}


			window.display();
			TickDelta = TickDeltaTimer.getElapsedTime();
				TickDeltaTimer.restart();
		}
	}
	}
	catch (Poco::Exception& e)
	{
		std::cout << "Poco Error: " << e.what() << std::endl;
		system("pause");
	}
	catch (std::exception& e)
	{
		
		std::cout << "Error: "<< e.what() << std::endl;
		system("pause");
	}
	
    return 0;
}

