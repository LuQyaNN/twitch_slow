#pragma once
#include "SFML/Graphics.hpp"
#include <set>
class emoti
{
public:
	emoti(sf::Image& image,sf::Vector2f spawn_pos);
	void Tick(sf::Time& delta);
	
	void Spawn() { spawned = true; life_time.restart(); }
	bool isSpawned() { return spawned; }
	sf::Texture texture;
	sf::Sprite sprite;
	sf::Clock life_time;
	~emoti();
private:
	bool spawned = false;
};

